import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UploadImageService {

  url = 'http://localhost:3000/api/upload';

  constructor(private http : HttpClient) { }

  uploadImage(selectedFile) {
    const fd = new FormData();
    console.log(selectedFile.name);
    fd.append('image',selectedFile,selectedFile.name);
    this.http.post(this.url,fd)
      .subscribe( res => {
        console.log(res);
      });
    }    
}
