import { Component, OnInit } from '@angular/core';
import { Article } from '../article/article';

import { from, Subscription } from 'rxjs';
import { PanierService } from '../panier.service';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-panier-articles',
  templateUrl: './panier-articles.component.html',
  styleUrls: ['./panier-articles.component.scss']
})
export class PanierArticlesComponent implements OnInit {

  totalPrix: number;
  compteurArticle: number;
  panier: Article[];
  subscription: Subscription;

  constructor(public panierService: PanierService) { }

  ngOnInit() {
    this.subscription = this.panierService.panier.subscribe(
      (panier) => {
        this.panier = panier;
        this.calculTotalPrix(panier);
      }
    );
  }

  calculTotalPrix(panier: Article[]): void{
    this.totalPrix = 0;
    this.compteurArticle = 0;
    panier.forEach(article => {
      this.totalPrix+=article.prix;
      this.compteurArticle+=1;
    })
  }

  deleteArticle(id: number) {
    this.panierService.deleteArticle(id);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
