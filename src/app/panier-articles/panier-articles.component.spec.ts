import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanierArticlesComponent } from './panier-articles.component';

describe('PanierArticlesComponent', () => {
  let component: PanierArticlesComponent;
  let fixture: ComponentFixture<PanierArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanierArticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanierArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
