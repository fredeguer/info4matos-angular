import { TestBed } from '@angular/core/testing';

import { ConstructeurService } from './constructeur.service';

describe('ConstructeurService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConstructeurService = TestBed.get(ConstructeurService);
    expect(service).toBeTruthy();
  });
});
