import { Constructeur } from './constructeur/constructeur';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConstructeurService {

  baseUrl: string = "http://localhost:8080/viking-drakkar";
  constructor(private  http: HttpClient){}

   httpOptions = {
     headers: new HttpHeaders({
        'Content-type': 'application/json'
     }).set("Access-Control-Allow-Origin", "*")
   };

   getConstructeur(){
     return this.http.get<Constructeur[]>(this.baseUrl+"/constructeur", this.httpOptions).pipe(
       map(
         (jsonArray: Object[])=>jsonArray.map(jsonItem => Constructeur.fromJson(jsonItem))
       ));

   }
}
