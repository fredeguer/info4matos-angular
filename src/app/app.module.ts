
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListeArticlesComponent } from './article/liste-articles/liste-articles.component';
import { ListeCategorieComponent } from './categorie/liste-categorie/liste-categorie.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { PanierArticlesComponent } from './panier-articles/panier-articles.component';
import { ArticleFormComponent } from './article-form/article-form.component';
import { FooterComponent } from './footer/footer/footer.component';
import { RouterModule } from '@angular/router';
import { ListeConstructeursComponent } from './constructeur/liste-constructeurs/liste-constructeurs.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';


@NgModule({
  declarations: [
    AppComponent,
    ListeArticlesComponent,
    ListeCategorieComponent,
    HeaderComponent,
    PanierArticlesComponent,
    ArticleFormComponent,
    FooterComponent,
    ListeConstructeursComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]

})
export class AppModule { }
