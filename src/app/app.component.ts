import { Component } from '@angular/core';
import { PanierService } from './panier.service';
import { Subscription } from 'rxjs';
import { Article } from './article/article';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  panier: Article[];
  subscription: Subscription;

  constructor(public panierService: PanierService) { }

  ngOnInit() {
    this.subscription = this.panierService.panier.subscribe(
      (panier) => {
        this.panier = panier;
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
