
import { UploadImageService } from './../upload-image.service';
import { Article } from './../article/article';
import { Component, OnInit } from '@angular/core';
import { CategorieService } from '../categorie.service';
import { Categorie } from '../categorie/categorie';
import { Constructeur } from '../constructeur/constructeur';
import { ConstructeurService } from '../constructeur.service';
import { ArticleService } from '../article.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.scss']
})
export class ArticleFormComponent implements OnInit {

  article = new  Article();
  listeCategorie: Categorie[];
  submitted: boolean = false;
  listeConstructeur: Constructeur[];
  selectedFile : File;
  imgURL: any;
 

  constructor(private uploadImageServer: UploadImageService,private categorieService: CategorieService, private constructeurService: ConstructeurService, private articleService: ArticleService) { }

  ngOnInit() {
    this.categorieService.getCategories().subscribe(
      data => {
        this.listeCategorie = data;
      }
    );
    this.constructeurService.getConstructeur().subscribe(
      data => {
        this.listeConstructeur = data;
      }
    );
    this.article = new Article();
   
  }

  onSubmit(form: NgForm): void {
    this.submitted = true;
     console.log(form.value);
     console.log(form.value['categorie']); 
     
      if (form.valid) {
        //code pour mettre l'image sur le repertoir assets
        console.log("image selectionné: "+ this.selectedFile.name);
        this.uploadImageServer.uploadImage(this.selectedFile);


       //je doit definir une categorie et une constructeur et creer une objete article et le mettre tous les infos de mon formulaire 
        let constructeur = new Constructeur(8,"MSI");
        let categorie = new Categorie(1,"écran"); 
        this.article['categorie']= categorie;
        this.article['constructeur']=constructeur;
        this.article['cheminFichierImage']= "/assets/images/materiel/" + this.selectedFile.name;
        this.article['dateDeMiseEnLigne']+= "this.selectedFile.name";
        console.log("***********");

        console.log(this.article.designation);
        console.log(this.article.constructeur);

       this.articleService.createArticle(this.article).subscribe(
         (article: Article) => { 
           console.log('Created article:', article);
         },
        //  (error) => { this.error = error.message; }
       );
     }
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
  
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.selectedFile = event.target.files[0];
      reader.onload = (event) => { // called once readAsDataURL is completed
        this.imgURL = reader.result;
      }
  
    }
  }

  rechargeConstructeur(event){
    console.log(event.value);
  }


}
