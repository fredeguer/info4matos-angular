import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../article';
import { ArticleService } from 'src/app/article.service';
import { PanierService } from 'src/app/panier.service';

@Component({
  selector: 'app-liste-articles',
  templateUrl: './liste-articles.component.html',
  styleUrls: ['./liste-articles.component.scss']
})
export class ListeArticlesComponent implements OnInit {

  listeArticles: Article[];
  selectedArticle: Article;
  totalPrix: number;

  constructor(
    private articleService: ArticleService,
    private panierService: PanierService
    ) { }

  ngOnInit() {
    this.articleService.getArticles().subscribe(
      data => {
        this.listeArticles = data;
        this.selectedArticle = this.listeArticles[0];
      }
    );
  }

  onSelect (art: Article): void {
    this.selectedArticle = art;
  }

  addArticle(art: Article) {
    console.log(art);
    this.panierService.addArticle(art);
  }
}
