import { Constructeur } from './../constructeur/constructeur';
import { Categorie } from 'src/app/categorie/categorie';
export class Article {
    constructor(
        public id?: number,
        public designation?: string,
        public cheminFichierImage?: string,
        public prix?: number,
        public content?: string,
        public categorie?: Categorie,
        public constructeur?: Constructeur,
        public miseEnLigne?: Date,
        public stock?: number
    ) {}

    public static fromJson(json: Object): Article {
        return new Article(
            json['id'],
            json['designation'],
            json['cheminFichierImage'],
            json['prix'],
            json['content'],
            json['categorie'],
            json['constructeur'],
            // json['miseEnLigne'],
            new Date(json['miseEnLigne']),
            json['stock']
        );
    }
}