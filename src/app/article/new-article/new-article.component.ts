import { Article } from './../article';
import { ConstructeurService } from 'src/app/constructeur.service';
import { Constructeur } from './../../constructeur/constructeur';
import { Component, OnInit, Input } from '@angular/core';
import { Categorie } from 'src/app/categorie/categorie';
import { CategorieService } from 'src/app/categorie.service';
import { ArticleService } from 'src/app/article.service';
import { NgForm } from '@angular/forms';
import { __importDefault } from 'tslib';

@Component({
  selector: 'app-new-article',
  templateUrl: './new-article.component.html',
  styleUrls: ['./new-article.component.scss']
})
export class NewArticleComponent implements OnInit {

  article: Article ;
  submitted = false;
  error: string; 
  listeCategorie: Categorie[];
  listeConstructeur: Constructeur[];
  current: Date = new Date();
  

  constructor(private articleService: ArticleService, private categorieService: CategorieService, private constructeurService: ConstructeurService) { }

  ngOnInit() {
    this.categorieService.getCategories().subscribe(
      data => {
        this.listeCategorie = data;
      }
    );
    this.constructeurService.getConstructeur().subscribe(
      data => {
        this.listeConstructeur = data;
      }
    );

  }

  onSubmit(form: NgForm) {
    console.log(form.value);
     this.submitted = true;
    //  if (form.valid) {
    //    this.articleService.createArticle(this.article).subscribe(
    //      (article: Article) => { 
    //        console.log('Created article:', article);
          
    //      },
    //      (error) => { this.error = error.message; }
    //    );
    //  }
  } 

}
