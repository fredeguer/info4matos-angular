export class Categorie {

    constructor(public id: number, public nom: string){}

    public static fromJson(json: Object): Categorie {
        return new Categorie(
            json['id'],
            json['nom']
        );
    }
}