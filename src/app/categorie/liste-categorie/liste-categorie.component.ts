import { Component, OnInit, Input } from '@angular/core';
import { Categorie } from '../categorie';
import { CategorieService } from 'src/app/categorie.service';

@Component({
  selector: 'app-liste-categorie',
  templateUrl: './liste-categorie.component.html',
  styleUrls: ['./liste-categorie.component.scss']
})
export class ListeCategorieComponent implements OnInit {

  listeCategories: Categorie[];

  constructor(private categorieService: CategorieService) { }

  ngOnInit() {
    this.categorieService.getCategories().subscribe(data => {
      this.listeCategories = data;
    });
  }

}
