import { Constructeur } from './../constructeur';
import { Component, OnInit } from '@angular/core';
import { ConstructeurService } from 'src/app/constructeur.service';


@Component({
  selector: 'app-liste-constructeur',
  templateUrl: './liste-constructeur.component.html',
  styleUrls: ['./liste-constructeur.component.scss']
})
export class ListeConstructeurComponent implements OnInit {

  listeConstructeur: Constructeur[];

  constructor(private constructeurService: ConstructeurService) { }

  ngOnInit() {
    this.constructeurService.getConstructeur().subscribe(data => {
      this.listeConstructeur = data;
    });
  }

}
