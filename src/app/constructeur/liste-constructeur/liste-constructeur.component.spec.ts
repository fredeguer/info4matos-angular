import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeConstructeurComponent } from './liste-constructeur.component';

describe('ListeConstructeurComponent', () => {
  let component: ListeConstructeurComponent;
  let fixture: ComponentFixture<ListeConstructeurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeConstructeurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeConstructeurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
