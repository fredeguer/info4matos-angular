import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeConstructeursComponent } from './liste-constructeurs.component';

describe('ListeConstructeursComponent', () => {
  let component: ListeConstructeursComponent;
  let fixture: ComponentFixture<ListeConstructeursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeConstructeursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeConstructeursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
