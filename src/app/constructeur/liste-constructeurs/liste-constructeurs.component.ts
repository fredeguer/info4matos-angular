import { Component, OnInit } from '@angular/core';
import { ConstructeurService } from 'src/app/constructeur.service';
import { Constructeur } from '../constructeur';

@Component({
  selector: 'app-liste-constructeurs',
  templateUrl: './liste-constructeurs.component.html',
  styleUrls: ['./liste-constructeurs.component.scss']
})
export class ListeConstructeursComponent implements OnInit {

  listeConstructeurs: Constructeur[];
  
  constructor(private constructeurService: ConstructeurService) { }

  ngOnInit() {
    this.constructeurService.getConstructeur().subscribe(
      data => {
        this.listeConstructeurs = data;
      }
    );
  }

}
