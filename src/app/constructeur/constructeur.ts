export class Constructeur {

    constructor(
        public id: number,
        public nom: string
    ) {}

    public static fromJson(json: Object): Constructeur {
        return new Constructeur(
            json['id'],
            json['nom']
        );
    }
}