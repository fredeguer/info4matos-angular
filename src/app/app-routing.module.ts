import { ListeArticlesComponent } from './article/liste-articles/liste-articles.component';
import { ArticleFormComponent } from './article-form/article-form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/liste-articles', pathMatch: 'full' },
  { path: 'articleFormulaire', component: ArticleFormComponent },
  { path: 'liste-articles', component: ListeArticlesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
