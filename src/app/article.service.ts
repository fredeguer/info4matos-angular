import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Article } from './article/article';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  baseUrl: string = "http://localhost:8080/viking-drakkar";
  constructor(private  http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
       'Content-type': 'application/json'
    })
  };

  getArticles() {
    return this.http.get<Article[]>(this.baseUrl+"/article", this.httpOptions).pipe(
      map(
        (jsonArray: Object[])=>jsonArray.map(jsonItem => Article.fromJson(jsonItem))
      ));
  }

  createArticle(article: Article): Observable<Article> {
    return this.http.post<Article>(this.baseUrl+"/article", article, this.httpOptions)
  }


}
